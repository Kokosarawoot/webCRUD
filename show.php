<?php
include 'db.php';

$sql = "SELECT * FROM users";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
  // output data of each row
  while($row = $result->fetch_assoc()) {
    echo "id: " . $row["id"]. " - Name: " . $row["fname"]. " " . $row["lname"]. "<br>";
  }
} else {
  echo "0 results";
}
$conn->close();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
    table, td, th {
    border: 1px solid;
    }

    table {
    width: 30%;
    border-collapse: collapse;
    }
    </style>
</head>
<body>
    <table>
        <tr>
            <td>ID</td>
            <td>ชื่อ</td>
            <td>นามสกุล</td>
            <td>อายุ</td>
            <td>จัดการ</td>
        </tr>
        <?php if ($result->num_rows > 0) :?>
        <?php foreach ($result as $row) :?>
        <tr>
            <td><?php echo $row['id']?></td>
            <td><?php echo $row['fname']?></td>
            <td><?php echo $row['lname']?></td>
            <td><?php echo $row['age']?></td>
            <td>
                <a href="form_insert.php">เพิ่มข้อมูล</a>
                <a href="form_update.php?id=<?php echo $row['id']?>">แก้ไข</a>
                <a href="deletedata.php?id=<?php echo $row['id']?>" onclick="return confirm('คุณต้องการลบข้อมูล?')">ลบ</a>
            </td>
        </tr>
        <?php endforeach?>
        <?php endif?>
    </table>
</body>
</html>